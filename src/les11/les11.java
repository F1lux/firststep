package les11;

import java.util.Formatter;
import java.util.Scanner;

public class les11 {
    private static Formatter frmtr;
    private static Scanner scn;
    public static void main (String[] args) {
        try {
            frmtr = new Formatter("res//les11.txt");
            scn = new Scanner(System.in);
            System.out.println("How are you old?");
            int ageP=(int)Double.parseDouble(scn.next());
            System.out.println("What your name?");
            String nameP=scn.next();
            System.out.println("Where you live?");
            String placeP=scn.next();
            frmtr.format("My name is %s, i'm %d years old, and live in %s",nameP,ageP,placeP);
            frmtr.close();
        } catch (Exception ignored) { }
    }
}
