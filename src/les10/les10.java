package les10;

import javax.swing.*;
import java.util.Scanner;
import java.io.File;

// Read from file
public class les10 {
    private static final String[][] mat = new String[5][3];
    private static Scanner scn;
    public static void main (String[] args) {
        openFile();
        readFile();
        out();
    }

    private static void out() {
        for (String[] strings : mat) {
            for (String string : strings) {
                System.out.print(string + " ");
            }
            System.out.println();
        }
    }

    private static void readFile() {
        while (scn.hasNext()) {
            for (int r=0;r<mat.length;r++) {
                for (int c=0;c<mat[r].length;c++) {
                    mat[r][c] = scn.next();
                }
            }
        }
    }

    private static void openFile() {
        try {
            scn = new Scanner(new File("res//1.txt"));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "File not found!");
        }
    }
}
