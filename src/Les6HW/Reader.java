package Les6HW;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Reader extends JFrame {
    JButton b1,b2;
    JLabel l1,l2,l3,l4;
    JTextField t1,t2;
    int a,b;
    String s1,s2;

        public Reader(String s) {
            super(s);
            setLayout(new FlowLayout());
            b1 = new JButton("Clear");
            b2 = new JButton("Calculate");
            l1 = new JLabel("Input first num:");
            l2 = new JLabel("Input second num:");
            l3 = new JLabel("");
            l4 = new JLabel("");
            t1 = new JTextField(10);
            t2 = new JTextField(10);
            add(t1);
            add(l1);
            add(t2);
            add(l2);
            add(l3);
            add(l4);
            add(b2);
            add(b1);
        }
}
