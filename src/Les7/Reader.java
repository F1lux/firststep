package Les7;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Reader extends JFrame {
    JButton b1,b2;
    JLabel l1,l2,l3,l4;
    JTextField t1,t2;
    int a,b;
    String s1,s2;
    eHandler handler = new eHandler();

    public Reader(String s) {
        super(s);
        setLayout(new FlowLayout());
        b1 = new JButton("Clear");
        b2 = new JButton("Calculate");
        l1 = new JLabel("Input first num:");
        l2 = new JLabel("Input second num:");
        l3 = new JLabel("");
        l4 = new JLabel("");
        t1 = new JTextField(10);
        t2 = new JTextField(10);
        add(b1);
        add(b2);
        add(l1);
        add(t1);
        add(l2);
        add(t2);
        add(l3);
        add(l4);
        b2.addActionListener(handler);
        b1.addActionListener(handler);
    }

    public class eHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                if (e.getSource() == b2) {
                    a = Integer.parseInt(t1.getText());
                    b = Integer.parseInt(t2.getText());
                    a++;
                    b++;
                    s1 = "First num now: " + a;
                    s2 = "Second num now: " + b;
                    l3.setText(s1);
                    l4.setText(s2);
                }
                if (e.getSource() == b1) {
                    t1.setText(null);
                    t2.setText(null);
                    l3.setText("");
                    l4.setText("");
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,"Input number!");
            }
        }
    }
}
