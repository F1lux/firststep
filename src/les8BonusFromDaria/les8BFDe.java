package les8BonusFromDaria;
// Вставить число 42 между вторым и третьим элементами массива, остальные элементы массива сдвинуть вправо.
// Массив задать изначально большего размера, чтобы новое число там поместилось.
public class les8BFDe {
    public static void main(String args[]) {
        int[] arrI = {1, 2, 3, 4, 5, 6, 0};
        int ins = 42;
        for (int i = arrI.length - 1; i > 0; i--) {
            if (i > 2) {
                arrI[i] = arrI[i - 1];
            }
        }
        arrI[3] = ins;
        for (int value : arrI) {
            System.out.print(value + " ");
        }
    }
}
