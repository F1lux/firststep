package les8BonusFromDaria;
// Дано два массива и третий, длина котрого равна длине первых двух. Сначала записать первый массив в третий, а затем -- второй после первого, в третий.
// Пример:
// 1-ый массив: 1,2,3
// 2-ой массив: 9,8
// До начала работы программы 3-ий массив: 0,0,0,0,0
// После работы программы 3-ий массив: 1,2,3,9,8
public class les8BFDf {
    public static void main (String args[]) {
        int[] arr1 = {1,2,3,4,10};
        int[] arr2 = {9,8,7,6,5};
        int[] arr3 = new int[arr1.length+arr2.length];
        System.out.println("Arr1:");
        for (int value : arr1) {
            System.out.print(value + " ");
        }
        System.out.println();
        System.out.println("Arr2:");
        for (int value : arr2) {
            System.out.print(value + " ");
        }
        System.out.println();
        System.out.println("Arr3:");
        for (int value : arr3) {
            System.out.print(value + " ");
        }
        for (int i=0;i<arr1.length;i++) {
            arr3[i]=arr1[i];
        }
        for (int i=0;i<arr2.length;i++) {
            arr3[arr1.length+i]=arr2[i];
        }
        System.out.println();
        System.out.println("Concatenation...");
        System.out.println("Arr3:");
        for (int value : arr3) {
            System.out.print(value + " ");
        }

    }
}
