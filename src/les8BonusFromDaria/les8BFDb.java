package les8BonusFromDaria;
// В массиве найти максимальный элемент с четным индексом. Вывести индекс и само число.
public class les8BFDb {
    private static final String messageMax = "Max number: ";
    private static final String messagePos = " even position: ";

    public static void main(String args[]) {
        int[] arrF = {1, 2, 8, 0, -2, -7, 9, 4, 3, -6, 5};
        int maxN, indPos = 0;
        maxN = arrF[0];
        for (int i = 0; i < arrF.length; i++) {
            if (i % 2 == 0) {
                if (arrF[i] > maxN) {
                    maxN = arrF[i];
                    indPos = i;
                }
            }
        }
        System.out.println(messageMax + maxN + messagePos + indPos);
    }
}
