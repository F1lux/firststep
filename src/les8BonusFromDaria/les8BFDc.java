package les8BonusFromDaria;
// Поменять местами минимальный и максимальный элементы. Вывести их индексы и сами числа.
public class les8BFDc {
    private static final String messageMin = "Minimal: ";
    private static final String messageMax = " Maximal: ";
    private static final String messagePos = " Position: ";

    public static void main(String args[]) {
        int[] arrS = {-1, -2, -3, 0, 1, 2, 3, 9, 8, 7, 6};
        int minN, maxN, minPos = 0, maxPos = 0;
        for (int arr : arrS) {
            System.out.print(arr + " ");
        }
        minN = arrS[0];
        maxN = arrS[0];
        for (int i = 0; i < arrS.length; i++) {
            if (arrS[i] > maxN) {
                maxN = arrS[i];
                maxPos = i;
            }
            if (arrS[i] < minN) {
                minN = arrS[i];
                minPos = i;
            }
        }
        System.out.println(messageMin + minN + messagePos + minPos + messageMax + maxN + messagePos + maxPos);
        System.out.println("Swap...");
        arrS[maxPos] = minN;
        arrS[minPos] = maxN;
        minN = arrS[0];
        maxN = arrS[0];
        minPos = 0;
        maxPos = 0;

        for (int arr : arrS) {
            System.out.print(arr + " ");
        }
        for (int i = 0; i < arrS.length; i++) {
            if (arrS[i] > maxN) {
                maxN = arrS[i];
                maxPos = i;
            }
            if (arrS[i] < minN) {
                minN = arrS[i];
                minPos = i;
            }
        }
        System.out.println(messageMin + minN + messagePos + minPos + messageMax + maxN + messagePos + maxPos);
    }
}
