package les8BonusFromDaria;
// Переставить элементы заданного массива в обратном порядке, то есть произвести реверс массива. Новых массивов не использовать.
public class les8BFDd {
    public static void main(String args[]) {
        int[] arrR = {7, 5, 3, 4, 1, -6, 73, 81, 0};
        int buf;
        for (int value : arrR) {
            System.out.print(value + " ");
        }
        System.out.println();
        System.out.println("Revers...");
        for (int i = 0; i < arrR.length / 2; i++) {
            buf = arrR[i];
            arrR[i] = arrR[arrR.length - i - 1];
            arrR[arrR.length - i - 1] = buf;
        }
        for (int value : arrR) {
            System.out.print(value + " ");
        }

    }
}
