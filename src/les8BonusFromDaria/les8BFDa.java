package les8BonusFromDaria;
//Найти два наименьших (минимальных) элемента массива. Вывести их индексы и сами числа.
public class les8BFDa {
    private static final String messageMin1 = "Minimal first: ";
    private static final String messageMin2 = " Minimal second: ";
    private static final String messagePos = " Position: ";

    public static void main(String[] args) {
        int[] arrF = {-9, 4, -4, 0, -2, -6, 1, 5};
        int minNum1, minNum2, indPos1 = 0, indPos2 = 0;
        minNum1 = 0;
        minNum2 = 0;
        for (int i = 0; i < arrF.length; i++) {
            if (arrF[i] < minNum1) {
                //noinspection ConstantConditions
                minNum2 = minNum1;
                indPos2 = i;
                minNum1 = arrF[i];
                indPos1 = i;
            } else if (arrF[i] < minNum2) {
                minNum2=arrF[i];
                indPos2=i;
            }
        }
        System.out.println(messageMin1 + minNum1 + messagePos + indPos1 + messageMin2 + minNum2 + messagePos + indPos2);
    }
}
