package les12;

import java.util.Random;

public class thrd implements Runnable{
    private final int time;
    private final String name;

    thrd(String name) {
        this.name = name;
        Random rand = new Random();
        time = rand.nextInt(999);
    }
    public void run() {
        System.out.printf("Thread %s sleep %d\n",name,time);
        try {
            Thread.sleep(time);
        } catch (Exception ignored) {
        }
        System.out.printf("Thread %s awake\n",name);
    }
}
