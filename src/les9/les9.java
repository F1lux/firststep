package les9;

public class les9 {
    public static void main(String[] args) {
        int[][] mat = {{1, 2, 3}, {5, 6}, {7, 8, 9}};
        for (int[] ints : mat) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }
}
