package by.ss.les22;

import java.util.ArrayList;
import java.util.Random;

class Worker {
    private ArrayList<Integer> list1 = new ArrayList<>();
    private ArrayList<Integer> list2 = new ArrayList<>();
    private final Object lock1 = new Object();
    private final Object lock2 = new Object();
    private Random random = new Random();

    private void partOne() {
        synchronized (lock1) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            list1.add(random.nextInt(100));
        }


    }
    private synchronized void partTwo() {
        synchronized (lock2) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            list2.add(random.nextInt(100));
        }

    }
    private void proceed() {
        for (int i=0;i<1000;i++) {
            partOne();
            partTwo();
        }
    }
    void start() {
        System.out.println("Start...");
        long startTime = System.currentTimeMillis();
        Thread t1 = new Thread(() -> proceed());
        Thread t2 = new Thread(() -> proceed());
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: "+(endTime-startTime)+"\n"+ "List 1: "+ list1.size()+"\n"+"List 2: "+list1.size());
    }
}
