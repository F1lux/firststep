package by.ss.les14;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.swing.*;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class Les14 extends JFrame implements Runnable  {
    static private Socket conn;
    static private ObjectOutputStream output;
    static private ObjectInputStream input;

    public static void main (String[] args) {
        new Thread(new Les14("Test")).start();
        new Thread(new Les14Server()).start();
    }

    public Les14 (String name) {
        super(name);
        setLayout(new FlowLayout());
        setSize(300,300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setLocationRelativeTo(null);

        final JTextField txtFld1 = new JTextField(10);
        final JButton butt1 = new JButton("Send");
        butt1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == butt1) {
                    sendData(txtFld1.getText());
                }
            }
        });
        add(txtFld1);
        add(butt1);
    }
    public void run() {
        try {
            while (true){
                conn = new Socket(InetAddress.getByName("127.0.0.1"),5678);
                output = new ObjectOutputStream(conn.getOutputStream());
                input = new ObjectInputStream(conn.getInputStream());
                JOptionPane.showMessageDialog(null, input.readObject());
            }
        } catch (ClassNotFoundException | HeadlessException | IOException e) {
        }

    }
    @SuppressWarnings("unused")
    private static void sendData (Object obj) {
        try {
            output.flush();
            output.writeObject(obj);
        } catch (IOException e) {}
    }
}
