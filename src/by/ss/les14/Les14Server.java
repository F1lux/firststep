package by.ss.les14;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Les14Server implements Runnable {
    static private ServerSocket serv;
    static private Socket conne;
    static private ObjectOutputStream output;
    static private ObjectInputStream input;

    public void run() {
        try {
            serv = new ServerSocket(5678, 10);
            while (true) {
                conne = serv.accept();
                output = new ObjectOutputStream(conne.getOutputStream());
                input = new ObjectInputStream(conne.getInputStream());
                JOptionPane.showMessageDialog(null, input.readObject());
                output.writeObject("Your message: " + input.readObject());
            }
        } catch (ClassNotFoundException | HeadlessException | IOException e) {
        }
    }
}
