package by.ss.les23;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

public class Les23 {
    private static ArrayBlockingQueue<Object> lst = new ArrayBlockingQueue<>(1000);

    public static void main(String[] args) {
        addObject();
        proceed();
    }

    private static void addObject() {
        for (int x = 0; x < 5; x++) {
            lst.add(new Object());
        }
    }

    private static void proceed() {
        System.out.println(lst.size());

            for(Object elem: lst) {
               lst.remove(elem);
        }
        System.out.println(lst.size());
    }
}
