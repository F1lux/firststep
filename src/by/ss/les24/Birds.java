package by.ss.les24;

public class Birds {
    private String name;
    private boolean canFly;

    Birds(String name, boolean canFly) {
        this.name=name;
        this.canFly=canFly;
    }


    String getName() {
        return name;
    }

    boolean isCanFly() {
        return canFly;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCanFly(boolean canFly) {
        this.canFly = canFly;
    }
}
