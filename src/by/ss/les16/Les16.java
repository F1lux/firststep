package by.ss.les16;

import java.util.ArrayList;
import java.util.Random;

public class Les16 {
    private static ArrayList<Phone> phones = new ArrayList<>();
    private static Random rand = new Random();

    public static void main(String[] args) {
        for (int i = 0; i < 300; i++) {
            phones.add(new Phone(rand.nextInt(9999999), "PekaPhone"));
        }
        for (Phone p : phones) {
            System.out.println(p.getNumber() + " " + p.getName());
        }
    }
}
