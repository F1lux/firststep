package by.ss.les16;

public class Phone {
    private int number;
    private String name;

    Phone(int number, String name) {
        this.number = number;
        this.name = name;
    }

    int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
