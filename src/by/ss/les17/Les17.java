package by.ss.les17;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;

public class Les17 {
    private static ArrayList<Profile> prof = new ArrayList<>();
    public static void main (String[] args) {
        prof = (ArrayList<Profile>) deserData("profiles");
        System.out.println(prof.size());
        Profile prfl = new Profile();
        prfl.setName(JOptionPane.showInputDialog(null,"Input your name"));
        prfl.setSurname(JOptionPane.showInputDialog(null,"Input your surname"));
        prof.add(prfl);
        for (Profile p: prof) {
            System.out.println(p.getName()+" "+p.getSurname());
        }
        System.out.println(prof.size());
        serData("profiles",prof);
    }

    private static Object deserData(String file_name) {
        Object retObj = null;
        try {
            FileInputStream filein = new FileInputStream(file_name + ".ser");
            ObjectInputStream In = new ObjectInputStream(filein);
            retObj = In.readObject();
            filein.close();
            In.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            System.exit(1);
        } catch (IOException e) {
            System.out.println("Error input/output");
            System.exit(2);
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
            System.exit(3);
        }
        return retObj;
    }

    private static void serData(String filename, Object obj) {
        try {
            FileOutputStream fileout = new FileOutputStream(filename + ".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileout);
            out.writeObject(obj);
            fileout.close();
            out.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            System.exit(1);
        } catch (IOException e) {
            System.out.println("Error input/output");
            System.exit(2);
        }
    }


}
