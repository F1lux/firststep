package by.ss.les25;

import java.util.ArrayList;

public class Les25 {
    public static void main(String[] args) {
        // NullPointerException
        //ArrayList<Object> lst = new ArrayList<>();
        //lst.add(new Object());
        //System.out.println(lst.size());

        //ArrayIndexOutOfBoundsException
        //int[] array = new int[5];
        //for (int i = 0; i < array.length; i++) {
        //    array[i] = i;
        //}
        //for (int i : array) {
        //    System.out.print(array[i] + " ");
        //}

        //StackOverFlowError
        recurse(10);


    }

    private static void recurse(int i) {
        recurse(i);
    }
}
