package Les7HW;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Objects;

public class Reader extends JFrame{
    private final JButton butt0;
    private final JButton butt1;
    private final JButton butt2;
    private final JButton butt3;
    private final JButton butt4;
    private final JButton butt5;
    private final JButton butt6;
    private final JButton butt7;
    private final JButton butt8;
    private final JButton butt9;
    private final JButton buttPlus;
    private final JButton buttMinus;
    private final JButton buttMultiply;
    private final JButton buttDiv;
    private final JButton buttChange;
    private final JButton buttEqually;
    private final JTextField txtField;
    private int a;
    private String ss;
    private boolean boolPlus = false, boolMtpl = false, boolMinus = false, boolDiv = false, boolOper = false;

    public Reader(String s) {
        super(s);
        setLayout(new FlowLayout());
        butt0 = new JButton("0");
        butt1 = new JButton("1");
        butt2 = new JButton("2");
        butt3 = new JButton("3");
        butt4 = new JButton("4");
        butt5 = new JButton("5");
        butt6 = new JButton("6");
        butt7 = new JButton("7");
        butt8 = new JButton("8");
        butt9 = new JButton("9");
        buttPlus = new JButton("+");
        buttMinus = new JButton("-");
        buttMultiply = new JButton("*");
        buttDiv = new JButton("/");
        buttChange = new JButton("+/-");
        buttEqually = new JButton("=");
        txtField = new JTextField("0",15);
        add(txtField);
        add(butt7);
        add(butt8);
        add(butt9);
        add(buttDiv);
        add(butt4);
        add(butt5);
        add(butt6);
        add(buttMultiply);
        add(butt1);
        add(butt2);
        add(butt3);
        add(buttMinus);
        add(buttChange);
        add(butt0);
        add(buttEqually);
        add(buttPlus);
        txtField.setEnabled(false);
        eHandler handler = new eHandler();
        butt0.addActionListener(handler);
        butt1.addActionListener(handler);
        butt2.addActionListener(handler);
        butt3.addActionListener(handler);
        butt4.addActionListener(handler);
        butt5.addActionListener(handler);
        butt6.addActionListener(handler);
        butt7.addActionListener(handler);
        butt8.addActionListener(handler);
        butt9.addActionListener(handler);
        buttPlus.addActionListener(handler);
        buttMinus.addActionListener(handler);
        buttMultiply.addActionListener(handler);
        buttDiv.addActionListener(handler);
        buttEqually.addActionListener(handler);
        buttChange.addActionListener(handler);
    }

    class eHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == butt0) {
                txtField.setText(inputNum(txtField.getText(),butt0.getText()));
            }
            if (e.getSource() == butt1) {
                txtField.setText(inputNum(txtField.getText(),butt1.getText()));
            }
            if (e.getSource() == butt2) {
                txtField.setText(inputNum(txtField.getText(),butt2.getText()));
            }
            if (e.getSource() == butt3) {
                txtField.setText(inputNum(txtField.getText(),butt3.getText()));
            }
            if (e.getSource() == butt4) {
                txtField.setText(inputNum(txtField.getText(),butt4.getText()));
            }
            if (e.getSource() == butt5) {
                txtField.setText(inputNum(txtField.getText(),butt5.getText()));
            }
            if (e.getSource() == butt6) {
                txtField.setText(inputNum(txtField.getText(),butt6.getText()));
            }
            if (e.getSource() == butt7) {
                txtField.setText(inputNum(txtField.getText(),butt7.getText()));
            }
            if (e.getSource() == butt8) {
                txtField.setText(inputNum(txtField.getText(),butt8.getText()));
            }
            if (e.getSource() == butt9) {
                txtField.setText(inputNum(txtField.getText(),butt9.getText()));
            }
            if (e.getSource() == buttPlus) {
                checkOper(txtField.getText(),buttPlus.getText());
            }
            if (e.getSource() == buttMinus) {
                checkOper(txtField.getText(),buttMinus.getText());
            }
            if (e.getSource() == buttMultiply) {
                checkOper(txtField.getText(),buttMultiply.getText());
            }
            if (e.getSource() == buttDiv) {
                checkOper(txtField.getText(),buttDiv.getText());
            }
            if (e.getSource() == buttEqually) {
                txtField.setText(resultEqu(txtField.getText()));
            }
            if (e.getSource() == buttChange) {
                txtField.setText(changePolus(txtField.getText()));
            }
        }
    }

    private String inputNum (String txtFldNum, String btnNum) {
        char ch;
        if (boolPlus || boolMinus || boolMtpl || boolDiv) {
            if (boolOper) {
                txtFldNum = btnNum;
                boolOper = false;
            } else {
                ch = txtFldNum.charAt(0);
                if (ch == '0') {
                    txtFldNum = btnNum;
                } else {
                    txtFldNum = txtFldNum + btnNum;
                }
            }
        } else {
            ch = txtFldNum.charAt(0);
            if (ch == '0') {
                txtFldNum = btnNum;
            } else {
                txtFldNum = txtFldNum + btnNum;
            }
        }
        return txtFldNum;
    }
    private void checkOper (String txtFldNum, String operSymb) {
        a = Integer.parseInt(txtFldNum);
        if (Objects.equals(operSymb, "+")) {
            boolOper = true;
            boolPlus = true;
            boolMinus = false;
            boolMtpl = false;
            boolDiv = false;
        }
        if (Objects.equals(operSymb, "-")) {
            boolOper = true;
            boolPlus = false;
            boolMinus = true;
            boolMtpl = false;
            boolDiv = false;
        }
        if (Objects.equals(operSymb, "*")) {
            boolOper = true;
            boolPlus = false;
            boolMinus = false;
            boolMtpl = true;
            boolDiv = false;
        }
        if (Objects.equals(operSymb, "/")) {
            boolOper = true;
            boolPlus = false;
            boolMinus = false;
            boolMtpl = false;
            boolDiv = true;
        }
    }
    private String resultEqu (String txtFldNum) {
        int buf;
        int b = Integer.parseInt(txtFldNum);
        if (boolPlus) {
            buf = a + b;
            txtFldNum = Integer.toString(buf);
        }
        if (boolMinus) {
            buf = a - b;
            txtFldNum = Integer.toString(buf);
        }
        if (boolMtpl) {
            buf = a * b;
            txtFldNum = Integer.toString(buf);
        }
        if (boolDiv) {
            buf = a / b;
            txtFldNum = Integer.toString(buf);
        }
        boolPlus = false;
        boolMinus = false;
        boolMtpl = false;
        boolDiv = false;
        boolOper = false;
        return txtFldNum;
    }
    private String changePolus (String txtFldNum) {
        char ch = txtFldNum.charAt(0);
        if (ch != '0') {
            int c = Integer.parseInt(txtFldNum);
            c = c * -1;
            txtFldNum = Integer.toString(c);
        }
        return txtFldNum;
    }
}

